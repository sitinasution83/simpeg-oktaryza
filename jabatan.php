<?php include 'header.php'; ?>
<h1>Jabatan</h1>

<a href="formJabatan.php" class="btn btn-info btn-sm mb-3 mt-3">Tambah</a>

<table class="table">
    <thead class="table-info">
        <tr>
            <th>ID Jabatan</th>
            <th>Jabatan</th>
			<?php if($_SESSION) : ?>
            <th>Aksi</th>
			<?php endif; ?>
        </tr>

    </thead>
    <tbody>


        <?php
        $sql = 'SELECT * FROM jabatan';
        $query = mysqli_query($conn, $sql);

        while ($row = mysqli_fetch_object($query)) {
        ?>

            <tr>
                <td> <?php echo $row->id_jabatan; ?> </td>
                <td><?php echo $row->nama_jabatan; ?></td>
				<?php if($_SESSION) : ?>
                <td>
                    <a href="formJabatan.php?id_jabatan=<?php echo $row->id_jabatan; ?> " class="btn btn-warning btn-sm">Edit</a>
                    <a href="deleteJabatan.php?id_jabatan=<?php echo $row->id_jabatan; ?> " class="btn btn-danger btn-sm" onclick="return confirm('Hapus data Jabatan?');">Hapus</a>
                </td>
				<?php endif;?>
            </tr>

        <?php
        }

        if (!mysqli_num_rows($query)) {
            echo '<tr><td colspan="8" class="text-center">Tidak ada data.</td></tr>';
        }
        ?>
    </tbody>
</table>

<?php include 'footer.php'; ?>